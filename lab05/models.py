from django.db import models
from django.utils import timezone

class Schedule(models.Model):
    activity_name = models.TextField()
    location = models.CharField(max_length=200)
    category = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()