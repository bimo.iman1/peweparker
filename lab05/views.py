from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import PostSchedule
from .models import Schedule
# Create your views here.
response={}
def index(request):
    form = PostSchedule()
    response['form'] = form
    return render(request,'Schedule.html', response)

def show_result(request):
    schedule = Schedule.objects.all()
    response['schedule']=schedule
    return render(request,'SchedResult.html', response)

def post(request):
    form = PostSchedule(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['activity_name'] = request.POST['activity_name']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        response['start_time'] = request.POST['start_time']
        response['end_time'] = request.POST['end_time']
        schedule = Schedule(
            activity_name = response['activity_name'],
            location = response['location'],
            category = response['category'],
            start_time = response['start_time'],
            end_time = response['end_time'])
        schedule.save()
        return HttpResponseRedirect('/schedule/result')
    else:
        return HttpResponseRedirect('/')

def delete_all(request):
    schedule = Schedule.objects.all()
    schedule.delete()
    return HttpResponseRedirect('/schedule/result')
