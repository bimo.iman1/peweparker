from django.conf.urls import url
from django.urls import path

from . import views

app_name = "lab05"

urlpatterns = [
    path('scheduling/', views.index, name='index'),
    path('result/',views.show_result, name='result'),
    path('process/',views.post, name='formprocess'),
    path('delete/',views.delete_all, name='deleteall'),
    #path('result/', views.sched_table,name='sched table'),
]
