from django import forms

class PostSchedule(forms.Form):
    attrs={'required': True, 'class': 'form-control'}
    attr_time={'type': 'date', 'required': True, 'class': 'form-control'}
    activity_name = forms.CharField(max_length=50)
    location = forms.CharField(max_length=254)
    category = forms.CharField(max_length=70)                         
    start_time = forms.DateTimeField(required = True)
    end_time = forms.DateTimeField(required = True)