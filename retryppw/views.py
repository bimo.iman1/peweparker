from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,"ppwHome.html")

def story_1(request):
    return render(request, "story1.html")

def story_4(request):
    return render(request,"Profile.html")

def register(request):
    return render(request,'Regist Form.html')

def claptrap(request):
    return render(request,'Claptrap.html')
