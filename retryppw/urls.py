from django.conf.urls import url
from django.urls import path

from . import views

app_name = "retryppw"

urlpatterns = [
    path('', views.home, name='home'),
    path('story_1/', views.story_1, name = 'story_1'),
    path('story_4/', views.story_4, name='story_4'),
    path('story_4/register/', views.register, name='register'),
    path('story_4/claptrap/', views.claptrap, name='claptrap'),
]
